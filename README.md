# WDYM

**W**hat **D**o **Y**ou **M**ean is a small web application that aims
help communities, teams, and organizations quickly understand domain
specific terms. If you've ever read or heard a term that you didn't
understand and felt the frustration of searching with a general
search engine, then this tool might be for you!

## Example

Let's say you're in a chat at work and you see someone mention the COOL
protocol. If you want to find out what this protocol stands for you can
ask WDYM and receive a list of terms defined by your workplace like so:

    COOL

    ----

    - COOL is a new protocol designed for fault tolerance.

    - COOL is the protocol used to escalate a SEV.

If you see this list, but don't feel that this accurately describes what
the conversation is about, then you can add an additional definition.

## Usage

Build and start the HTTP server using Docker.

    # From the root of the repository
    docker build -f Dockerfile -t wdym .
    mkdir ~/.cache/wdym
    docker run -d --name wdym --restart=unless-stopped -p '8080:8080' -v ~/.cache/wdym:/app/db wdym

Once it's running you'll be able to view and create definitions for terms.

### Creating a definition

To create a term, visit the `/create` page and submit the form with the
term and definition.

### Viewing definitions

To view definitions, visit the `/search` page and provide a term to search for
in the `q` parameter. Ex: `/search?q=WDYM`.

## Work in progress

- Authenticated SQLite access.
- OIDC for authentication.
- Include business context.
- Allow users to like definitions.
- Fuzzy search terms.
- Manage previously submitted definitions.
- Integration with [Litestream](https://litestream.io/).

## Known issues

- There is no method for migrating the data at the moment.

## License

See the [License](/LICENSE.md) file.

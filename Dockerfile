# syntax=docker/dockerfile:1
FROM docker.io/library/golang:1.20.4-alpine AS builder

WORKDIR /app

COPY go.mod go.sum .

ENV CGO_ENABLED=1

RUN <<EOT
go mod download
apk -q --no-cache add build-base
go install github.com/mattn/go-sqlite3
EOT

COPY . .

RUN go build -o bin/wdym ./cmd/wdym

FROM docker.io/library/alpine:3.17.3

WORKDIR /app

COPY migrations/ migrations/
COPY public/ public/

RUN <<EOT
mkdir -p bin/ db/
apk -q --no-cache add wget
wget -O atlas -q https://release.ariga.io/atlas/atlas-linux-amd64-musl-latest
chmod +x atlas
./atlas migrate apply --dir "file://migrations" --url "sqlite://db/test.db?_fk=1"
rm -rfv migrations atlas
apk -q del wget
EOT

COPY --from=builder /app/bin/wdym bin/

ENTRYPOINT ["/app/bin/wdym"]

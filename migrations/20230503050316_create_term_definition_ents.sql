-- Create "definitions" table
CREATE TABLE `definitions` (`id` integer NOT NULL PRIMARY KEY AUTOINCREMENT, `created_at` datetime NOT NULL, `updated_at` datetime NOT NULL, `text` text NOT NULL, `term_definitions` integer NULL, CONSTRAINT `definitions_terms_definitions` FOREIGN KEY (`term_definitions`) REFERENCES `terms` (`id`) ON DELETE SET NULL);
-- Create index "definition_text_term_definitions" to table: "definitions"
CREATE UNIQUE INDEX `definition_text_term_definitions` ON `definitions` (`text`, `term_definitions`);
-- Create "terms" table
CREATE TABLE `terms` (`id` integer NOT NULL PRIMARY KEY AUTOINCREMENT, `created_at` datetime NOT NULL, `updated_at` datetime NOT NULL, `text` text NOT NULL);
-- Create index "terms_text_key" to table: "terms"
CREATE UNIQUE INDEX `terms_text_key` ON `terms` (`text`);
-- Create index "term_text" to table: "terms"
CREATE INDEX `term_text` ON `terms` (`text`);

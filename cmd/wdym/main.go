package main

import (
	"errors"
	"flag"
	"html/template"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/oscar.alberto.tovar/wdym/ent"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/term"
)

var r = &TemplateRender{
	templates: template.Must(template.ParseGlob("public/views/*.html")),
}

type TemplateRender struct {
	templates *template.Template
}

type input struct {
	Term        ent.Term
	Definitions []*ent.Definition
}

func (t *TemplateRender) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

var flagAddr = flag.String("addr", ":8080", "addr for http server to listen on")

func main() {
	flag.Parse()

	entclient, err := ent.Open("sqlite3", "file:db/test.db?_journal=WAL")
	if err != nil {
		log.Fatalf("Opening sqlite3 entclient: %s", err)
	}

	e := echo.New()
	e.Renderer = r
	e.Use(
		middleware.Gzip(),
		middleware.Decompress(),
		middleware.Logger(),
	)

	e.GET("/create", func(c echo.Context) error {
		return c.Render(http.StatusOK, "create definition", nil)
	})

	e.POST("/create", func(c echo.Context) error {
		ctx := c.Request().Context()
		termText := c.FormValue("term")
		definitionText := c.FormValue("definition")
		tx, err := entclient.Tx(ctx)
		if err != nil {
			log.Println("Could not begin tx to create term and definition:", err)
			return c.NoContent(http.StatusInternalServerError)
		}

		termID, err := tx.Term.
			Create().
			SetText(strings.ToLower(termText)).
			OnConflictColumns(term.FieldText).
			Ignore().
			ID(ctx)
		if err != nil {
			log.Println("Could not create term:", errors.Join(err, tx.Rollback()))
			return c.NoContent(http.StatusInternalServerError)
		}

		err = tx.Definition.
			Create().
			SetText(definitionText).
			SetTermID(termID).
			Exec(ctx)

		// If unique constraint fails that's okay, but
		// we should still show the non-duplicated entry
		// to the user.
		if err != nil && !ent.IsConstraintError(err) {
			log.Println("Could not create definition:", errors.Join(err, tx.Rollback()))
			return c.NoContent(http.StatusInternalServerError)
		}

		if err := tx.Commit(); err != nil {
			log.Println("Could not commit tx to create term and definition:", errors.Join(err, tx.Rollback()))
			return c.NoContent(http.StatusInternalServerError)
		}
		return c.Render(http.StatusOK, "view definitions", input{
			Term: ent.Term{
				Text: termText,
			},
			Definitions: []*ent.Definition{{Text: definitionText}},
		})
	})

	e.GET("/search", func(c echo.Context) error {
		ctx := c.Request().Context()
		q := c.QueryParam("q")
		definitions, err := entclient.Term.
			Query().
			Where(
				term.Text(strings.ToLower(q)),
			).
			QueryDefinitions().
			All(ctx)
		if err != nil {
			log.Printf("Could not query definitions for %q: %s", q, err)
		}

		return c.Render(http.StatusOK, "view definitions", input{
			Term: ent.Term{
				Text: q,
			},
			Definitions: definitions,
		})
	})

	if err := e.Start(*flagAddr); err != nil {
		log.Fatalf("Starting http server: %s", err)
	}
}

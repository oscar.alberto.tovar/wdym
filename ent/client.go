// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"log"

	"gitlab.com/oscar.alberto.tovar/wdym/ent/migrate"

	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/definition"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/term"
)

// Client is the client that holds all ent builders.
type Client struct {
	config
	// Schema is the client for creating, migrating and dropping schema.
	Schema *migrate.Schema
	// Definition is the client for interacting with the Definition builders.
	Definition *DefinitionClient
	// Term is the client for interacting with the Term builders.
	Term *TermClient
}

// NewClient creates a new client configured with the given options.
func NewClient(opts ...Option) *Client {
	cfg := config{log: log.Println, hooks: &hooks{}, inters: &inters{}}
	cfg.options(opts...)
	client := &Client{config: cfg}
	client.init()
	return client
}

func (c *Client) init() {
	c.Schema = migrate.NewSchema(c.driver)
	c.Definition = NewDefinitionClient(c.config)
	c.Term = NewTermClient(c.config)
}

type (
	// config is the configuration for the client and its builder.
	config struct {
		// driver used for executing database requests.
		driver dialect.Driver
		// debug enable a debug logging.
		debug bool
		// log used for logging on debug mode.
		log func(...any)
		// hooks to execute on mutations.
		hooks *hooks
		// interceptors to execute on queries.
		inters *inters
	}
	// Option function to configure the client.
	Option func(*config)
)

// options applies the options on the config object.
func (c *config) options(opts ...Option) {
	for _, opt := range opts {
		opt(c)
	}
	if c.debug {
		c.driver = dialect.Debug(c.driver, c.log)
	}
}

// Debug enables debug logging on the ent.Driver.
func Debug() Option {
	return func(c *config) {
		c.debug = true
	}
}

// Log sets the logging function for debug mode.
func Log(fn func(...any)) Option {
	return func(c *config) {
		c.log = fn
	}
}

// Driver configures the client driver.
func Driver(driver dialect.Driver) Option {
	return func(c *config) {
		c.driver = driver
	}
}

// Open opens a database/sql.DB specified by the driver name and
// the data source name, and returns a new client attached to it.
// Optional parameters can be added for configuring the client.
func Open(driverName, dataSourceName string, options ...Option) (*Client, error) {
	switch driverName {
	case dialect.MySQL, dialect.Postgres, dialect.SQLite:
		drv, err := sql.Open(driverName, dataSourceName)
		if err != nil {
			return nil, err
		}
		return NewClient(append(options, Driver(drv))...), nil
	default:
		return nil, fmt.Errorf("unsupported driver: %q", driverName)
	}
}

// Tx returns a new transactional client. The provided context
// is used until the transaction is committed or rolled back.
func (c *Client) Tx(ctx context.Context) (*Tx, error) {
	if _, ok := c.driver.(*txDriver); ok {
		return nil, errors.New("ent: cannot start a transaction within a transaction")
	}
	tx, err := newTx(ctx, c.driver)
	if err != nil {
		return nil, fmt.Errorf("ent: starting a transaction: %w", err)
	}
	cfg := c.config
	cfg.driver = tx
	return &Tx{
		ctx:        ctx,
		config:     cfg,
		Definition: NewDefinitionClient(cfg),
		Term:       NewTermClient(cfg),
	}, nil
}

// BeginTx returns a transactional client with specified options.
func (c *Client) BeginTx(ctx context.Context, opts *sql.TxOptions) (*Tx, error) {
	if _, ok := c.driver.(*txDriver); ok {
		return nil, errors.New("ent: cannot start a transaction within a transaction")
	}
	tx, err := c.driver.(interface {
		BeginTx(context.Context, *sql.TxOptions) (dialect.Tx, error)
	}).BeginTx(ctx, opts)
	if err != nil {
		return nil, fmt.Errorf("ent: starting a transaction: %w", err)
	}
	cfg := c.config
	cfg.driver = &txDriver{tx: tx, drv: c.driver}
	return &Tx{
		ctx:        ctx,
		config:     cfg,
		Definition: NewDefinitionClient(cfg),
		Term:       NewTermClient(cfg),
	}, nil
}

// Debug returns a new debug-client. It's used to get verbose logging on specific operations.
//
//	client.Debug().
//		Definition.
//		Query().
//		Count(ctx)
func (c *Client) Debug() *Client {
	if c.debug {
		return c
	}
	cfg := c.config
	cfg.driver = dialect.Debug(c.driver, c.log)
	client := &Client{config: cfg}
	client.init()
	return client
}

// Close closes the database connection and prevents new queries from starting.
func (c *Client) Close() error {
	return c.driver.Close()
}

// Use adds the mutation hooks to all the entity clients.
// In order to add hooks to a specific client, call: `client.Node.Use(...)`.
func (c *Client) Use(hooks ...Hook) {
	c.Definition.Use(hooks...)
	c.Term.Use(hooks...)
}

// Intercept adds the query interceptors to all the entity clients.
// In order to add interceptors to a specific client, call: `client.Node.Intercept(...)`.
func (c *Client) Intercept(interceptors ...Interceptor) {
	c.Definition.Intercept(interceptors...)
	c.Term.Intercept(interceptors...)
}

// Mutate implements the ent.Mutator interface.
func (c *Client) Mutate(ctx context.Context, m Mutation) (Value, error) {
	switch m := m.(type) {
	case *DefinitionMutation:
		return c.Definition.mutate(ctx, m)
	case *TermMutation:
		return c.Term.mutate(ctx, m)
	default:
		return nil, fmt.Errorf("ent: unknown mutation type %T", m)
	}
}

// DefinitionClient is a client for the Definition schema.
type DefinitionClient struct {
	config
}

// NewDefinitionClient returns a client for the Definition from the given config.
func NewDefinitionClient(c config) *DefinitionClient {
	return &DefinitionClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `definition.Hooks(f(g(h())))`.
func (c *DefinitionClient) Use(hooks ...Hook) {
	c.hooks.Definition = append(c.hooks.Definition, hooks...)
}

// Intercept adds a list of query interceptors to the interceptors stack.
// A call to `Intercept(f, g, h)` equals to `definition.Intercept(f(g(h())))`.
func (c *DefinitionClient) Intercept(interceptors ...Interceptor) {
	c.inters.Definition = append(c.inters.Definition, interceptors...)
}

// Create returns a builder for creating a Definition entity.
func (c *DefinitionClient) Create() *DefinitionCreate {
	mutation := newDefinitionMutation(c.config, OpCreate)
	return &DefinitionCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of Definition entities.
func (c *DefinitionClient) CreateBulk(builders ...*DefinitionCreate) *DefinitionCreateBulk {
	return &DefinitionCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for Definition.
func (c *DefinitionClient) Update() *DefinitionUpdate {
	mutation := newDefinitionMutation(c.config, OpUpdate)
	return &DefinitionUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *DefinitionClient) UpdateOne(d *Definition) *DefinitionUpdateOne {
	mutation := newDefinitionMutation(c.config, OpUpdateOne, withDefinition(d))
	return &DefinitionUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *DefinitionClient) UpdateOneID(id int) *DefinitionUpdateOne {
	mutation := newDefinitionMutation(c.config, OpUpdateOne, withDefinitionID(id))
	return &DefinitionUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for Definition.
func (c *DefinitionClient) Delete() *DefinitionDelete {
	mutation := newDefinitionMutation(c.config, OpDelete)
	return &DefinitionDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a builder for deleting the given entity.
func (c *DefinitionClient) DeleteOne(d *Definition) *DefinitionDeleteOne {
	return c.DeleteOneID(d.ID)
}

// DeleteOneID returns a builder for deleting the given entity by its id.
func (c *DefinitionClient) DeleteOneID(id int) *DefinitionDeleteOne {
	builder := c.Delete().Where(definition.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &DefinitionDeleteOne{builder}
}

// Query returns a query builder for Definition.
func (c *DefinitionClient) Query() *DefinitionQuery {
	return &DefinitionQuery{
		config: c.config,
		ctx:    &QueryContext{Type: TypeDefinition},
		inters: c.Interceptors(),
	}
}

// Get returns a Definition entity by its id.
func (c *DefinitionClient) Get(ctx context.Context, id int) (*Definition, error) {
	return c.Query().Where(definition.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *DefinitionClient) GetX(ctx context.Context, id int) *Definition {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// QueryTerm queries the term edge of a Definition.
func (c *DefinitionClient) QueryTerm(d *Definition) *TermQuery {
	query := (&TermClient{config: c.config}).Query()
	query.path = func(context.Context) (fromV *sql.Selector, _ error) {
		id := d.ID
		step := sqlgraph.NewStep(
			sqlgraph.From(definition.Table, definition.FieldID, id),
			sqlgraph.To(term.Table, term.FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, definition.TermTable, definition.TermColumn),
		)
		fromV = sqlgraph.Neighbors(d.driver.Dialect(), step)
		return fromV, nil
	}
	return query
}

// Hooks returns the client hooks.
func (c *DefinitionClient) Hooks() []Hook {
	return c.hooks.Definition
}

// Interceptors returns the client interceptors.
func (c *DefinitionClient) Interceptors() []Interceptor {
	return c.inters.Definition
}

func (c *DefinitionClient) mutate(ctx context.Context, m *DefinitionMutation) (Value, error) {
	switch m.Op() {
	case OpCreate:
		return (&DefinitionCreate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdate:
		return (&DefinitionUpdate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdateOne:
		return (&DefinitionUpdateOne{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpDelete, OpDeleteOne:
		return (&DefinitionDelete{config: c.config, hooks: c.Hooks(), mutation: m}).Exec(ctx)
	default:
		return nil, fmt.Errorf("ent: unknown Definition mutation op: %q", m.Op())
	}
}

// TermClient is a client for the Term schema.
type TermClient struct {
	config
}

// NewTermClient returns a client for the Term from the given config.
func NewTermClient(c config) *TermClient {
	return &TermClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `term.Hooks(f(g(h())))`.
func (c *TermClient) Use(hooks ...Hook) {
	c.hooks.Term = append(c.hooks.Term, hooks...)
}

// Intercept adds a list of query interceptors to the interceptors stack.
// A call to `Intercept(f, g, h)` equals to `term.Intercept(f(g(h())))`.
func (c *TermClient) Intercept(interceptors ...Interceptor) {
	c.inters.Term = append(c.inters.Term, interceptors...)
}

// Create returns a builder for creating a Term entity.
func (c *TermClient) Create() *TermCreate {
	mutation := newTermMutation(c.config, OpCreate)
	return &TermCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of Term entities.
func (c *TermClient) CreateBulk(builders ...*TermCreate) *TermCreateBulk {
	return &TermCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for Term.
func (c *TermClient) Update() *TermUpdate {
	mutation := newTermMutation(c.config, OpUpdate)
	return &TermUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *TermClient) UpdateOne(t *Term) *TermUpdateOne {
	mutation := newTermMutation(c.config, OpUpdateOne, withTerm(t))
	return &TermUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *TermClient) UpdateOneID(id int) *TermUpdateOne {
	mutation := newTermMutation(c.config, OpUpdateOne, withTermID(id))
	return &TermUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for Term.
func (c *TermClient) Delete() *TermDelete {
	mutation := newTermMutation(c.config, OpDelete)
	return &TermDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a builder for deleting the given entity.
func (c *TermClient) DeleteOne(t *Term) *TermDeleteOne {
	return c.DeleteOneID(t.ID)
}

// DeleteOneID returns a builder for deleting the given entity by its id.
func (c *TermClient) DeleteOneID(id int) *TermDeleteOne {
	builder := c.Delete().Where(term.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &TermDeleteOne{builder}
}

// Query returns a query builder for Term.
func (c *TermClient) Query() *TermQuery {
	return &TermQuery{
		config: c.config,
		ctx:    &QueryContext{Type: TypeTerm},
		inters: c.Interceptors(),
	}
}

// Get returns a Term entity by its id.
func (c *TermClient) Get(ctx context.Context, id int) (*Term, error) {
	return c.Query().Where(term.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *TermClient) GetX(ctx context.Context, id int) *Term {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// QueryDefinitions queries the definitions edge of a Term.
func (c *TermClient) QueryDefinitions(t *Term) *DefinitionQuery {
	query := (&DefinitionClient{config: c.config}).Query()
	query.path = func(context.Context) (fromV *sql.Selector, _ error) {
		id := t.ID
		step := sqlgraph.NewStep(
			sqlgraph.From(term.Table, term.FieldID, id),
			sqlgraph.To(definition.Table, definition.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, false, term.DefinitionsTable, term.DefinitionsColumn),
		)
		fromV = sqlgraph.Neighbors(t.driver.Dialect(), step)
		return fromV, nil
	}
	return query
}

// Hooks returns the client hooks.
func (c *TermClient) Hooks() []Hook {
	return c.hooks.Term
}

// Interceptors returns the client interceptors.
func (c *TermClient) Interceptors() []Interceptor {
	return c.inters.Term
}

func (c *TermClient) mutate(ctx context.Context, m *TermMutation) (Value, error) {
	switch m.Op() {
	case OpCreate:
		return (&TermCreate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdate:
		return (&TermUpdate{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpUpdateOne:
		return (&TermUpdateOne{config: c.config, hooks: c.Hooks(), mutation: m}).Save(ctx)
	case OpDelete, OpDeleteOne:
		return (&TermDelete{config: c.config, hooks: c.Hooks(), mutation: m}).Exec(ctx)
	default:
		return nil, fmt.Errorf("ent: unknown Term mutation op: %q", m.Op())
	}
}

// hooks and interceptors per client, for fast access.
type (
	hooks struct {
		Definition, Term []ent.Hook
	}
	inters struct {
		Definition, Term []ent.Interceptor
	}
)

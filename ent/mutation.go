// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/sql"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/definition"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/predicate"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/term"
)

const (
	// Operation types.
	OpCreate    = ent.OpCreate
	OpDelete    = ent.OpDelete
	OpDeleteOne = ent.OpDeleteOne
	OpUpdate    = ent.OpUpdate
	OpUpdateOne = ent.OpUpdateOne

	// Node types.
	TypeDefinition = "Definition"
	TypeTerm       = "Term"
)

// DefinitionMutation represents an operation that mutates the Definition nodes in the graph.
type DefinitionMutation struct {
	config
	op            Op
	typ           string
	id            *int
	created_at    *time.Time
	updated_at    *time.Time
	text          *string
	clearedFields map[string]struct{}
	term          *int
	clearedterm   bool
	done          bool
	oldValue      func(context.Context) (*Definition, error)
	predicates    []predicate.Definition
}

var _ ent.Mutation = (*DefinitionMutation)(nil)

// definitionOption allows management of the mutation configuration using functional options.
type definitionOption func(*DefinitionMutation)

// newDefinitionMutation creates new mutation for the Definition entity.
func newDefinitionMutation(c config, op Op, opts ...definitionOption) *DefinitionMutation {
	m := &DefinitionMutation{
		config:        c,
		op:            op,
		typ:           TypeDefinition,
		clearedFields: make(map[string]struct{}),
	}
	for _, opt := range opts {
		opt(m)
	}
	return m
}

// withDefinitionID sets the ID field of the mutation.
func withDefinitionID(id int) definitionOption {
	return func(m *DefinitionMutation) {
		var (
			err   error
			once  sync.Once
			value *Definition
		)
		m.oldValue = func(ctx context.Context) (*Definition, error) {
			once.Do(func() {
				if m.done {
					err = errors.New("querying old values post mutation is not allowed")
				} else {
					value, err = m.Client().Definition.Get(ctx, id)
				}
			})
			return value, err
		}
		m.id = &id
	}
}

// withDefinition sets the old Definition of the mutation.
func withDefinition(node *Definition) definitionOption {
	return func(m *DefinitionMutation) {
		m.oldValue = func(context.Context) (*Definition, error) {
			return node, nil
		}
		m.id = &node.ID
	}
}

// Client returns a new `ent.Client` from the mutation. If the mutation was
// executed in a transaction (ent.Tx), a transactional client is returned.
func (m DefinitionMutation) Client() *Client {
	client := &Client{config: m.config}
	client.init()
	return client
}

// Tx returns an `ent.Tx` for mutations that were executed in transactions;
// it returns an error otherwise.
func (m DefinitionMutation) Tx() (*Tx, error) {
	if _, ok := m.driver.(*txDriver); !ok {
		return nil, errors.New("ent: mutation is not running in a transaction")
	}
	tx := &Tx{config: m.config}
	tx.init()
	return tx, nil
}

// ID returns the ID value in the mutation. Note that the ID is only available
// if it was provided to the builder or after it was returned from the database.
func (m *DefinitionMutation) ID() (id int, exists bool) {
	if m.id == nil {
		return
	}
	return *m.id, true
}

// IDs queries the database and returns the entity ids that match the mutation's predicate.
// That means, if the mutation is applied within a transaction with an isolation level such
// as sql.LevelSerializable, the returned ids match the ids of the rows that will be updated
// or updated by the mutation.
func (m *DefinitionMutation) IDs(ctx context.Context) ([]int, error) {
	switch {
	case m.op.Is(OpUpdateOne | OpDeleteOne):
		id, exists := m.ID()
		if exists {
			return []int{id}, nil
		}
		fallthrough
	case m.op.Is(OpUpdate | OpDelete):
		return m.Client().Definition.Query().Where(m.predicates...).IDs(ctx)
	default:
		return nil, fmt.Errorf("IDs is not allowed on %s operations", m.op)
	}
}

// SetCreatedAt sets the "created_at" field.
func (m *DefinitionMutation) SetCreatedAt(t time.Time) {
	m.created_at = &t
}

// CreatedAt returns the value of the "created_at" field in the mutation.
func (m *DefinitionMutation) CreatedAt() (r time.Time, exists bool) {
	v := m.created_at
	if v == nil {
		return
	}
	return *v, true
}

// OldCreatedAt returns the old "created_at" field's value of the Definition entity.
// If the Definition object wasn't provided to the builder, the object is fetched from the database.
// An error is returned if the mutation operation is not UpdateOne, or the database query fails.
func (m *DefinitionMutation) OldCreatedAt(ctx context.Context) (v time.Time, err error) {
	if !m.op.Is(OpUpdateOne) {
		return v, errors.New("OldCreatedAt is only allowed on UpdateOne operations")
	}
	if m.id == nil || m.oldValue == nil {
		return v, errors.New("OldCreatedAt requires an ID field in the mutation")
	}
	oldValue, err := m.oldValue(ctx)
	if err != nil {
		return v, fmt.Errorf("querying old value for OldCreatedAt: %w", err)
	}
	return oldValue.CreatedAt, nil
}

// ResetCreatedAt resets all changes to the "created_at" field.
func (m *DefinitionMutation) ResetCreatedAt() {
	m.created_at = nil
}

// SetUpdatedAt sets the "updated_at" field.
func (m *DefinitionMutation) SetUpdatedAt(t time.Time) {
	m.updated_at = &t
}

// UpdatedAt returns the value of the "updated_at" field in the mutation.
func (m *DefinitionMutation) UpdatedAt() (r time.Time, exists bool) {
	v := m.updated_at
	if v == nil {
		return
	}
	return *v, true
}

// OldUpdatedAt returns the old "updated_at" field's value of the Definition entity.
// If the Definition object wasn't provided to the builder, the object is fetched from the database.
// An error is returned if the mutation operation is not UpdateOne, or the database query fails.
func (m *DefinitionMutation) OldUpdatedAt(ctx context.Context) (v time.Time, err error) {
	if !m.op.Is(OpUpdateOne) {
		return v, errors.New("OldUpdatedAt is only allowed on UpdateOne operations")
	}
	if m.id == nil || m.oldValue == nil {
		return v, errors.New("OldUpdatedAt requires an ID field in the mutation")
	}
	oldValue, err := m.oldValue(ctx)
	if err != nil {
		return v, fmt.Errorf("querying old value for OldUpdatedAt: %w", err)
	}
	return oldValue.UpdatedAt, nil
}

// ResetUpdatedAt resets all changes to the "updated_at" field.
func (m *DefinitionMutation) ResetUpdatedAt() {
	m.updated_at = nil
}

// SetText sets the "text" field.
func (m *DefinitionMutation) SetText(s string) {
	m.text = &s
}

// Text returns the value of the "text" field in the mutation.
func (m *DefinitionMutation) Text() (r string, exists bool) {
	v := m.text
	if v == nil {
		return
	}
	return *v, true
}

// OldText returns the old "text" field's value of the Definition entity.
// If the Definition object wasn't provided to the builder, the object is fetched from the database.
// An error is returned if the mutation operation is not UpdateOne, or the database query fails.
func (m *DefinitionMutation) OldText(ctx context.Context) (v string, err error) {
	if !m.op.Is(OpUpdateOne) {
		return v, errors.New("OldText is only allowed on UpdateOne operations")
	}
	if m.id == nil || m.oldValue == nil {
		return v, errors.New("OldText requires an ID field in the mutation")
	}
	oldValue, err := m.oldValue(ctx)
	if err != nil {
		return v, fmt.Errorf("querying old value for OldText: %w", err)
	}
	return oldValue.Text, nil
}

// ResetText resets all changes to the "text" field.
func (m *DefinitionMutation) ResetText() {
	m.text = nil
}

// SetTermID sets the "term" edge to the Term entity by id.
func (m *DefinitionMutation) SetTermID(id int) {
	m.term = &id
}

// ClearTerm clears the "term" edge to the Term entity.
func (m *DefinitionMutation) ClearTerm() {
	m.clearedterm = true
}

// TermCleared reports if the "term" edge to the Term entity was cleared.
func (m *DefinitionMutation) TermCleared() bool {
	return m.clearedterm
}

// TermID returns the "term" edge ID in the mutation.
func (m *DefinitionMutation) TermID() (id int, exists bool) {
	if m.term != nil {
		return *m.term, true
	}
	return
}

// TermIDs returns the "term" edge IDs in the mutation.
// Note that IDs always returns len(IDs) <= 1 for unique edges, and you should use
// TermID instead. It exists only for internal usage by the builders.
func (m *DefinitionMutation) TermIDs() (ids []int) {
	if id := m.term; id != nil {
		ids = append(ids, *id)
	}
	return
}

// ResetTerm resets all changes to the "term" edge.
func (m *DefinitionMutation) ResetTerm() {
	m.term = nil
	m.clearedterm = false
}

// Where appends a list predicates to the DefinitionMutation builder.
func (m *DefinitionMutation) Where(ps ...predicate.Definition) {
	m.predicates = append(m.predicates, ps...)
}

// WhereP appends storage-level predicates to the DefinitionMutation builder. Using this method,
// users can use type-assertion to append predicates that do not depend on any generated package.
func (m *DefinitionMutation) WhereP(ps ...func(*sql.Selector)) {
	p := make([]predicate.Definition, len(ps))
	for i := range ps {
		p[i] = ps[i]
	}
	m.Where(p...)
}

// Op returns the operation name.
func (m *DefinitionMutation) Op() Op {
	return m.op
}

// SetOp allows setting the mutation operation.
func (m *DefinitionMutation) SetOp(op Op) {
	m.op = op
}

// Type returns the node type of this mutation (Definition).
func (m *DefinitionMutation) Type() string {
	return m.typ
}

// Fields returns all fields that were changed during this mutation. Note that in
// order to get all numeric fields that were incremented/decremented, call
// AddedFields().
func (m *DefinitionMutation) Fields() []string {
	fields := make([]string, 0, 3)
	if m.created_at != nil {
		fields = append(fields, definition.FieldCreatedAt)
	}
	if m.updated_at != nil {
		fields = append(fields, definition.FieldUpdatedAt)
	}
	if m.text != nil {
		fields = append(fields, definition.FieldText)
	}
	return fields
}

// Field returns the value of a field with the given name. The second boolean
// return value indicates that this field was not set, or was not defined in the
// schema.
func (m *DefinitionMutation) Field(name string) (ent.Value, bool) {
	switch name {
	case definition.FieldCreatedAt:
		return m.CreatedAt()
	case definition.FieldUpdatedAt:
		return m.UpdatedAt()
	case definition.FieldText:
		return m.Text()
	}
	return nil, false
}

// OldField returns the old value of the field from the database. An error is
// returned if the mutation operation is not UpdateOne, or the query to the
// database failed.
func (m *DefinitionMutation) OldField(ctx context.Context, name string) (ent.Value, error) {
	switch name {
	case definition.FieldCreatedAt:
		return m.OldCreatedAt(ctx)
	case definition.FieldUpdatedAt:
		return m.OldUpdatedAt(ctx)
	case definition.FieldText:
		return m.OldText(ctx)
	}
	return nil, fmt.Errorf("unknown Definition field %s", name)
}

// SetField sets the value of a field with the given name. It returns an error if
// the field is not defined in the schema, or if the type mismatched the field
// type.
func (m *DefinitionMutation) SetField(name string, value ent.Value) error {
	switch name {
	case definition.FieldCreatedAt:
		v, ok := value.(time.Time)
		if !ok {
			return fmt.Errorf("unexpected type %T for field %s", value, name)
		}
		m.SetCreatedAt(v)
		return nil
	case definition.FieldUpdatedAt:
		v, ok := value.(time.Time)
		if !ok {
			return fmt.Errorf("unexpected type %T for field %s", value, name)
		}
		m.SetUpdatedAt(v)
		return nil
	case definition.FieldText:
		v, ok := value.(string)
		if !ok {
			return fmt.Errorf("unexpected type %T for field %s", value, name)
		}
		m.SetText(v)
		return nil
	}
	return fmt.Errorf("unknown Definition field %s", name)
}

// AddedFields returns all numeric fields that were incremented/decremented during
// this mutation.
func (m *DefinitionMutation) AddedFields() []string {
	return nil
}

// AddedField returns the numeric value that was incremented/decremented on a field
// with the given name. The second boolean return value indicates that this field
// was not set, or was not defined in the schema.
func (m *DefinitionMutation) AddedField(name string) (ent.Value, bool) {
	return nil, false
}

// AddField adds the value to the field with the given name. It returns an error if
// the field is not defined in the schema, or if the type mismatched the field
// type.
func (m *DefinitionMutation) AddField(name string, value ent.Value) error {
	switch name {
	}
	return fmt.Errorf("unknown Definition numeric field %s", name)
}

// ClearedFields returns all nullable fields that were cleared during this
// mutation.
func (m *DefinitionMutation) ClearedFields() []string {
	return nil
}

// FieldCleared returns a boolean indicating if a field with the given name was
// cleared in this mutation.
func (m *DefinitionMutation) FieldCleared(name string) bool {
	_, ok := m.clearedFields[name]
	return ok
}

// ClearField clears the value of the field with the given name. It returns an
// error if the field is not defined in the schema.
func (m *DefinitionMutation) ClearField(name string) error {
	return fmt.Errorf("unknown Definition nullable field %s", name)
}

// ResetField resets all changes in the mutation for the field with the given name.
// It returns an error if the field is not defined in the schema.
func (m *DefinitionMutation) ResetField(name string) error {
	switch name {
	case definition.FieldCreatedAt:
		m.ResetCreatedAt()
		return nil
	case definition.FieldUpdatedAt:
		m.ResetUpdatedAt()
		return nil
	case definition.FieldText:
		m.ResetText()
		return nil
	}
	return fmt.Errorf("unknown Definition field %s", name)
}

// AddedEdges returns all edge names that were set/added in this mutation.
func (m *DefinitionMutation) AddedEdges() []string {
	edges := make([]string, 0, 1)
	if m.term != nil {
		edges = append(edges, definition.EdgeTerm)
	}
	return edges
}

// AddedIDs returns all IDs (to other nodes) that were added for the given edge
// name in this mutation.
func (m *DefinitionMutation) AddedIDs(name string) []ent.Value {
	switch name {
	case definition.EdgeTerm:
		if id := m.term; id != nil {
			return []ent.Value{*id}
		}
	}
	return nil
}

// RemovedEdges returns all edge names that were removed in this mutation.
func (m *DefinitionMutation) RemovedEdges() []string {
	edges := make([]string, 0, 1)
	return edges
}

// RemovedIDs returns all IDs (to other nodes) that were removed for the edge with
// the given name in this mutation.
func (m *DefinitionMutation) RemovedIDs(name string) []ent.Value {
	return nil
}

// ClearedEdges returns all edge names that were cleared in this mutation.
func (m *DefinitionMutation) ClearedEdges() []string {
	edges := make([]string, 0, 1)
	if m.clearedterm {
		edges = append(edges, definition.EdgeTerm)
	}
	return edges
}

// EdgeCleared returns a boolean which indicates if the edge with the given name
// was cleared in this mutation.
func (m *DefinitionMutation) EdgeCleared(name string) bool {
	switch name {
	case definition.EdgeTerm:
		return m.clearedterm
	}
	return false
}

// ClearEdge clears the value of the edge with the given name. It returns an error
// if that edge is not defined in the schema.
func (m *DefinitionMutation) ClearEdge(name string) error {
	switch name {
	case definition.EdgeTerm:
		m.ClearTerm()
		return nil
	}
	return fmt.Errorf("unknown Definition unique edge %s", name)
}

// ResetEdge resets all changes to the edge with the given name in this mutation.
// It returns an error if the edge is not defined in the schema.
func (m *DefinitionMutation) ResetEdge(name string) error {
	switch name {
	case definition.EdgeTerm:
		m.ResetTerm()
		return nil
	}
	return fmt.Errorf("unknown Definition edge %s", name)
}

// TermMutation represents an operation that mutates the Term nodes in the graph.
type TermMutation struct {
	config
	op                 Op
	typ                string
	id                 *int
	created_at         *time.Time
	updated_at         *time.Time
	text               *string
	clearedFields      map[string]struct{}
	definitions        map[int]struct{}
	removeddefinitions map[int]struct{}
	cleareddefinitions bool
	done               bool
	oldValue           func(context.Context) (*Term, error)
	predicates         []predicate.Term
}

var _ ent.Mutation = (*TermMutation)(nil)

// termOption allows management of the mutation configuration using functional options.
type termOption func(*TermMutation)

// newTermMutation creates new mutation for the Term entity.
func newTermMutation(c config, op Op, opts ...termOption) *TermMutation {
	m := &TermMutation{
		config:        c,
		op:            op,
		typ:           TypeTerm,
		clearedFields: make(map[string]struct{}),
	}
	for _, opt := range opts {
		opt(m)
	}
	return m
}

// withTermID sets the ID field of the mutation.
func withTermID(id int) termOption {
	return func(m *TermMutation) {
		var (
			err   error
			once  sync.Once
			value *Term
		)
		m.oldValue = func(ctx context.Context) (*Term, error) {
			once.Do(func() {
				if m.done {
					err = errors.New("querying old values post mutation is not allowed")
				} else {
					value, err = m.Client().Term.Get(ctx, id)
				}
			})
			return value, err
		}
		m.id = &id
	}
}

// withTerm sets the old Term of the mutation.
func withTerm(node *Term) termOption {
	return func(m *TermMutation) {
		m.oldValue = func(context.Context) (*Term, error) {
			return node, nil
		}
		m.id = &node.ID
	}
}

// Client returns a new `ent.Client` from the mutation. If the mutation was
// executed in a transaction (ent.Tx), a transactional client is returned.
func (m TermMutation) Client() *Client {
	client := &Client{config: m.config}
	client.init()
	return client
}

// Tx returns an `ent.Tx` for mutations that were executed in transactions;
// it returns an error otherwise.
func (m TermMutation) Tx() (*Tx, error) {
	if _, ok := m.driver.(*txDriver); !ok {
		return nil, errors.New("ent: mutation is not running in a transaction")
	}
	tx := &Tx{config: m.config}
	tx.init()
	return tx, nil
}

// ID returns the ID value in the mutation. Note that the ID is only available
// if it was provided to the builder or after it was returned from the database.
func (m *TermMutation) ID() (id int, exists bool) {
	if m.id == nil {
		return
	}
	return *m.id, true
}

// IDs queries the database and returns the entity ids that match the mutation's predicate.
// That means, if the mutation is applied within a transaction with an isolation level such
// as sql.LevelSerializable, the returned ids match the ids of the rows that will be updated
// or updated by the mutation.
func (m *TermMutation) IDs(ctx context.Context) ([]int, error) {
	switch {
	case m.op.Is(OpUpdateOne | OpDeleteOne):
		id, exists := m.ID()
		if exists {
			return []int{id}, nil
		}
		fallthrough
	case m.op.Is(OpUpdate | OpDelete):
		return m.Client().Term.Query().Where(m.predicates...).IDs(ctx)
	default:
		return nil, fmt.Errorf("IDs is not allowed on %s operations", m.op)
	}
}

// SetCreatedAt sets the "created_at" field.
func (m *TermMutation) SetCreatedAt(t time.Time) {
	m.created_at = &t
}

// CreatedAt returns the value of the "created_at" field in the mutation.
func (m *TermMutation) CreatedAt() (r time.Time, exists bool) {
	v := m.created_at
	if v == nil {
		return
	}
	return *v, true
}

// OldCreatedAt returns the old "created_at" field's value of the Term entity.
// If the Term object wasn't provided to the builder, the object is fetched from the database.
// An error is returned if the mutation operation is not UpdateOne, or the database query fails.
func (m *TermMutation) OldCreatedAt(ctx context.Context) (v time.Time, err error) {
	if !m.op.Is(OpUpdateOne) {
		return v, errors.New("OldCreatedAt is only allowed on UpdateOne operations")
	}
	if m.id == nil || m.oldValue == nil {
		return v, errors.New("OldCreatedAt requires an ID field in the mutation")
	}
	oldValue, err := m.oldValue(ctx)
	if err != nil {
		return v, fmt.Errorf("querying old value for OldCreatedAt: %w", err)
	}
	return oldValue.CreatedAt, nil
}

// ResetCreatedAt resets all changes to the "created_at" field.
func (m *TermMutation) ResetCreatedAt() {
	m.created_at = nil
}

// SetUpdatedAt sets the "updated_at" field.
func (m *TermMutation) SetUpdatedAt(t time.Time) {
	m.updated_at = &t
}

// UpdatedAt returns the value of the "updated_at" field in the mutation.
func (m *TermMutation) UpdatedAt() (r time.Time, exists bool) {
	v := m.updated_at
	if v == nil {
		return
	}
	return *v, true
}

// OldUpdatedAt returns the old "updated_at" field's value of the Term entity.
// If the Term object wasn't provided to the builder, the object is fetched from the database.
// An error is returned if the mutation operation is not UpdateOne, or the database query fails.
func (m *TermMutation) OldUpdatedAt(ctx context.Context) (v time.Time, err error) {
	if !m.op.Is(OpUpdateOne) {
		return v, errors.New("OldUpdatedAt is only allowed on UpdateOne operations")
	}
	if m.id == nil || m.oldValue == nil {
		return v, errors.New("OldUpdatedAt requires an ID field in the mutation")
	}
	oldValue, err := m.oldValue(ctx)
	if err != nil {
		return v, fmt.Errorf("querying old value for OldUpdatedAt: %w", err)
	}
	return oldValue.UpdatedAt, nil
}

// ResetUpdatedAt resets all changes to the "updated_at" field.
func (m *TermMutation) ResetUpdatedAt() {
	m.updated_at = nil
}

// SetText sets the "text" field.
func (m *TermMutation) SetText(s string) {
	m.text = &s
}

// Text returns the value of the "text" field in the mutation.
func (m *TermMutation) Text() (r string, exists bool) {
	v := m.text
	if v == nil {
		return
	}
	return *v, true
}

// OldText returns the old "text" field's value of the Term entity.
// If the Term object wasn't provided to the builder, the object is fetched from the database.
// An error is returned if the mutation operation is not UpdateOne, or the database query fails.
func (m *TermMutation) OldText(ctx context.Context) (v string, err error) {
	if !m.op.Is(OpUpdateOne) {
		return v, errors.New("OldText is only allowed on UpdateOne operations")
	}
	if m.id == nil || m.oldValue == nil {
		return v, errors.New("OldText requires an ID field in the mutation")
	}
	oldValue, err := m.oldValue(ctx)
	if err != nil {
		return v, fmt.Errorf("querying old value for OldText: %w", err)
	}
	return oldValue.Text, nil
}

// ResetText resets all changes to the "text" field.
func (m *TermMutation) ResetText() {
	m.text = nil
}

// AddDefinitionIDs adds the "definitions" edge to the Definition entity by ids.
func (m *TermMutation) AddDefinitionIDs(ids ...int) {
	if m.definitions == nil {
		m.definitions = make(map[int]struct{})
	}
	for i := range ids {
		m.definitions[ids[i]] = struct{}{}
	}
}

// ClearDefinitions clears the "definitions" edge to the Definition entity.
func (m *TermMutation) ClearDefinitions() {
	m.cleareddefinitions = true
}

// DefinitionsCleared reports if the "definitions" edge to the Definition entity was cleared.
func (m *TermMutation) DefinitionsCleared() bool {
	return m.cleareddefinitions
}

// RemoveDefinitionIDs removes the "definitions" edge to the Definition entity by IDs.
func (m *TermMutation) RemoveDefinitionIDs(ids ...int) {
	if m.removeddefinitions == nil {
		m.removeddefinitions = make(map[int]struct{})
	}
	for i := range ids {
		delete(m.definitions, ids[i])
		m.removeddefinitions[ids[i]] = struct{}{}
	}
}

// RemovedDefinitions returns the removed IDs of the "definitions" edge to the Definition entity.
func (m *TermMutation) RemovedDefinitionsIDs() (ids []int) {
	for id := range m.removeddefinitions {
		ids = append(ids, id)
	}
	return
}

// DefinitionsIDs returns the "definitions" edge IDs in the mutation.
func (m *TermMutation) DefinitionsIDs() (ids []int) {
	for id := range m.definitions {
		ids = append(ids, id)
	}
	return
}

// ResetDefinitions resets all changes to the "definitions" edge.
func (m *TermMutation) ResetDefinitions() {
	m.definitions = nil
	m.cleareddefinitions = false
	m.removeddefinitions = nil
}

// Where appends a list predicates to the TermMutation builder.
func (m *TermMutation) Where(ps ...predicate.Term) {
	m.predicates = append(m.predicates, ps...)
}

// WhereP appends storage-level predicates to the TermMutation builder. Using this method,
// users can use type-assertion to append predicates that do not depend on any generated package.
func (m *TermMutation) WhereP(ps ...func(*sql.Selector)) {
	p := make([]predicate.Term, len(ps))
	for i := range ps {
		p[i] = ps[i]
	}
	m.Where(p...)
}

// Op returns the operation name.
func (m *TermMutation) Op() Op {
	return m.op
}

// SetOp allows setting the mutation operation.
func (m *TermMutation) SetOp(op Op) {
	m.op = op
}

// Type returns the node type of this mutation (Term).
func (m *TermMutation) Type() string {
	return m.typ
}

// Fields returns all fields that were changed during this mutation. Note that in
// order to get all numeric fields that were incremented/decremented, call
// AddedFields().
func (m *TermMutation) Fields() []string {
	fields := make([]string, 0, 3)
	if m.created_at != nil {
		fields = append(fields, term.FieldCreatedAt)
	}
	if m.updated_at != nil {
		fields = append(fields, term.FieldUpdatedAt)
	}
	if m.text != nil {
		fields = append(fields, term.FieldText)
	}
	return fields
}

// Field returns the value of a field with the given name. The second boolean
// return value indicates that this field was not set, or was not defined in the
// schema.
func (m *TermMutation) Field(name string) (ent.Value, bool) {
	switch name {
	case term.FieldCreatedAt:
		return m.CreatedAt()
	case term.FieldUpdatedAt:
		return m.UpdatedAt()
	case term.FieldText:
		return m.Text()
	}
	return nil, false
}

// OldField returns the old value of the field from the database. An error is
// returned if the mutation operation is not UpdateOne, or the query to the
// database failed.
func (m *TermMutation) OldField(ctx context.Context, name string) (ent.Value, error) {
	switch name {
	case term.FieldCreatedAt:
		return m.OldCreatedAt(ctx)
	case term.FieldUpdatedAt:
		return m.OldUpdatedAt(ctx)
	case term.FieldText:
		return m.OldText(ctx)
	}
	return nil, fmt.Errorf("unknown Term field %s", name)
}

// SetField sets the value of a field with the given name. It returns an error if
// the field is not defined in the schema, or if the type mismatched the field
// type.
func (m *TermMutation) SetField(name string, value ent.Value) error {
	switch name {
	case term.FieldCreatedAt:
		v, ok := value.(time.Time)
		if !ok {
			return fmt.Errorf("unexpected type %T for field %s", value, name)
		}
		m.SetCreatedAt(v)
		return nil
	case term.FieldUpdatedAt:
		v, ok := value.(time.Time)
		if !ok {
			return fmt.Errorf("unexpected type %T for field %s", value, name)
		}
		m.SetUpdatedAt(v)
		return nil
	case term.FieldText:
		v, ok := value.(string)
		if !ok {
			return fmt.Errorf("unexpected type %T for field %s", value, name)
		}
		m.SetText(v)
		return nil
	}
	return fmt.Errorf("unknown Term field %s", name)
}

// AddedFields returns all numeric fields that were incremented/decremented during
// this mutation.
func (m *TermMutation) AddedFields() []string {
	return nil
}

// AddedField returns the numeric value that was incremented/decremented on a field
// with the given name. The second boolean return value indicates that this field
// was not set, or was not defined in the schema.
func (m *TermMutation) AddedField(name string) (ent.Value, bool) {
	return nil, false
}

// AddField adds the value to the field with the given name. It returns an error if
// the field is not defined in the schema, or if the type mismatched the field
// type.
func (m *TermMutation) AddField(name string, value ent.Value) error {
	switch name {
	}
	return fmt.Errorf("unknown Term numeric field %s", name)
}

// ClearedFields returns all nullable fields that were cleared during this
// mutation.
func (m *TermMutation) ClearedFields() []string {
	return nil
}

// FieldCleared returns a boolean indicating if a field with the given name was
// cleared in this mutation.
func (m *TermMutation) FieldCleared(name string) bool {
	_, ok := m.clearedFields[name]
	return ok
}

// ClearField clears the value of the field with the given name. It returns an
// error if the field is not defined in the schema.
func (m *TermMutation) ClearField(name string) error {
	return fmt.Errorf("unknown Term nullable field %s", name)
}

// ResetField resets all changes in the mutation for the field with the given name.
// It returns an error if the field is not defined in the schema.
func (m *TermMutation) ResetField(name string) error {
	switch name {
	case term.FieldCreatedAt:
		m.ResetCreatedAt()
		return nil
	case term.FieldUpdatedAt:
		m.ResetUpdatedAt()
		return nil
	case term.FieldText:
		m.ResetText()
		return nil
	}
	return fmt.Errorf("unknown Term field %s", name)
}

// AddedEdges returns all edge names that were set/added in this mutation.
func (m *TermMutation) AddedEdges() []string {
	edges := make([]string, 0, 1)
	if m.definitions != nil {
		edges = append(edges, term.EdgeDefinitions)
	}
	return edges
}

// AddedIDs returns all IDs (to other nodes) that were added for the given edge
// name in this mutation.
func (m *TermMutation) AddedIDs(name string) []ent.Value {
	switch name {
	case term.EdgeDefinitions:
		ids := make([]ent.Value, 0, len(m.definitions))
		for id := range m.definitions {
			ids = append(ids, id)
		}
		return ids
	}
	return nil
}

// RemovedEdges returns all edge names that were removed in this mutation.
func (m *TermMutation) RemovedEdges() []string {
	edges := make([]string, 0, 1)
	if m.removeddefinitions != nil {
		edges = append(edges, term.EdgeDefinitions)
	}
	return edges
}

// RemovedIDs returns all IDs (to other nodes) that were removed for the edge with
// the given name in this mutation.
func (m *TermMutation) RemovedIDs(name string) []ent.Value {
	switch name {
	case term.EdgeDefinitions:
		ids := make([]ent.Value, 0, len(m.removeddefinitions))
		for id := range m.removeddefinitions {
			ids = append(ids, id)
		}
		return ids
	}
	return nil
}

// ClearedEdges returns all edge names that were cleared in this mutation.
func (m *TermMutation) ClearedEdges() []string {
	edges := make([]string, 0, 1)
	if m.cleareddefinitions {
		edges = append(edges, term.EdgeDefinitions)
	}
	return edges
}

// EdgeCleared returns a boolean which indicates if the edge with the given name
// was cleared in this mutation.
func (m *TermMutation) EdgeCleared(name string) bool {
	switch name {
	case term.EdgeDefinitions:
		return m.cleareddefinitions
	}
	return false
}

// ClearEdge clears the value of the edge with the given name. It returns an error
// if that edge is not defined in the schema.
func (m *TermMutation) ClearEdge(name string) error {
	switch name {
	}
	return fmt.Errorf("unknown Term unique edge %s", name)
}

// ResetEdge resets all changes to the edge with the given name in this mutation.
// It returns an error if the edge is not defined in the schema.
func (m *TermMutation) ResetEdge(name string) error {
	switch name {
	case term.EdgeDefinitions:
		m.ResetDefinitions()
		return nil
	}
	return fmt.Errorf("unknown Term edge %s", name)
}

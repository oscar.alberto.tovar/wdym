// Code generated by ent, DO NOT EDIT.

package ent

import (
	"time"

	"gitlab.com/oscar.alberto.tovar/wdym/ent/definition"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/schema"
	"gitlab.com/oscar.alberto.tovar/wdym/ent/term"
)

// The init function reads all schema descriptors with runtime code
// (default values, validators, hooks and policies) and stitches it
// to their package variables.
func init() {
	definitionMixin := schema.Definition{}.Mixin()
	definitionMixinFields0 := definitionMixin[0].Fields()
	_ = definitionMixinFields0
	definitionFields := schema.Definition{}.Fields()
	_ = definitionFields
	// definitionDescCreatedAt is the schema descriptor for created_at field.
	definitionDescCreatedAt := definitionMixinFields0[0].Descriptor()
	// definition.DefaultCreatedAt holds the default value on creation for the created_at field.
	definition.DefaultCreatedAt = definitionDescCreatedAt.Default.(func() time.Time)
	// definitionDescUpdatedAt is the schema descriptor for updated_at field.
	definitionDescUpdatedAt := definitionMixinFields0[1].Descriptor()
	// definition.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	definition.DefaultUpdatedAt = definitionDescUpdatedAt.Default.(func() time.Time)
	// definition.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	definition.UpdateDefaultUpdatedAt = definitionDescUpdatedAt.UpdateDefault.(func() time.Time)
	// definitionDescText is the schema descriptor for text field.
	definitionDescText := definitionFields[0].Descriptor()
	// definition.TextValidator is a validator for the "text" field. It is called by the builders before save.
	definition.TextValidator = func() func(string) error {
		validators := definitionDescText.Validators
		fns := [...]func(string) error{
			validators[0].(func(string) error),
			validators[1].(func(string) error),
		}
		return func(text string) error {
			for _, fn := range fns {
				if err := fn(text); err != nil {
					return err
				}
			}
			return nil
		}
	}()
	termMixin := schema.Term{}.Mixin()
	termMixinFields0 := termMixin[0].Fields()
	_ = termMixinFields0
	termFields := schema.Term{}.Fields()
	_ = termFields
	// termDescCreatedAt is the schema descriptor for created_at field.
	termDescCreatedAt := termMixinFields0[0].Descriptor()
	// term.DefaultCreatedAt holds the default value on creation for the created_at field.
	term.DefaultCreatedAt = termDescCreatedAt.Default.(func() time.Time)
	// termDescUpdatedAt is the schema descriptor for updated_at field.
	termDescUpdatedAt := termMixinFields0[1].Descriptor()
	// term.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	term.DefaultUpdatedAt = termDescUpdatedAt.Default.(func() time.Time)
	// term.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	term.UpdateDefaultUpdatedAt = termDescUpdatedAt.UpdateDefault.(func() time.Time)
	// termDescText is the schema descriptor for text field.
	termDescText := termFields[0].Descriptor()
	// term.TextValidator is a validator for the "text" field. It is called by the builders before save.
	term.TextValidator = func() func(string) error {
		validators := termDescText.Validators
		fns := [...]func(string) error{
			validators[0].(func(string) error),
			validators[1].(func(string) error),
		}
		return func(text string) error {
			for _, fn := range fns {
				if err := fn(text); err != nil {
					return err
				}
			}
			return nil
		}
	}()
}

package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Definition holds the schema definition for the Definition entity.
type Definition struct {
	ent.Schema
}

func (Definition) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
	}
}

// Fields of the Definition.
func (Definition) Fields() []ent.Field {
	return []ent.Field{
		field.String("text").
			NotEmpty().
			MaxLen(256),
	}
}

// Edges of the Definition.
func (Definition) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("term", Term.Type).
			Ref("definitions").
			Unique(),
	}
}

func (Definition) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("text").
			Edges("term").
			Unique(),
	}
}

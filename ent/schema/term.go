package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Term holds the schema definition for the Term entity.
type Term struct {
	ent.Schema
}

func (Term) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
	}
}

// Fields of the Term.
func (Term) Fields() []ent.Field {
	return []ent.Field{
		field.String("text").
			Unique().
			NotEmpty().
			MaxLen(128),
	}
}

// Edges of the Term.
func (Term) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("definitions", Definition.Type),
	}
}

func (Term) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("text"),
	}
}
